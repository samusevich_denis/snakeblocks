﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class ChangeStateNode : Node
    {
        public override NodeState Evaluate()
        {
            //Debug.Log(this.gameObject.name);
            if (GameController.IsChangeState)
            {
                return NodeState.Success;
            }
            return NodeState.Failure;
        }
    }

}
