﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class MoveNode : Node
    {
        [SerializeField] private SnakeAI snake; 
        public override NodeState Evaluate()
        {
            //Debug.Log(this.gameObject.name);
            var directionVector = SnakeCreator.GetDirectionVector(snake.directionMove);
            snake.headSnake.position=snake.PositionHeadSnake-(directionVector-(Vector3)directionVector*GameController.Cycle);
            return NodeState.Success;
        }
    }
}

