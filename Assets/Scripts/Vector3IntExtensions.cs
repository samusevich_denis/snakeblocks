﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vector3IntExtensions
{
    public static Vector3 ClampVector3Int(this Vector3Int outVector, Vector3Int vector, Vector3Int vectorMin, Vector3Int vectorMax)
    {
        var x = Mathf.Clamp(vector.x, vectorMin.x, vectorMax.x);
        var y = Mathf.Clamp(vector.y, vectorMin.y, vectorMax.y);
        var z = Mathf.Clamp(vector.z, vectorMin.z, vectorMax.z);
        return new Vector3Int(x, y, z);
    }
    public static Vector3Int GetRandomVectorInt(this Vector3Int Vector3, Vector3Int vectorMin, Vector3Int vectorMax)
    {
        var x = Random.Range(vectorMin.x, vectorMax.x);
        var y = Random.Range(vectorMin.y, vectorMax.y);
        var z = Random.Range(vectorMin.z, vectorMax.z);
        return new Vector3Int(x, y, z);
    }

    public static Vector3Int GetRandomVectorInt(this Vector3Int Vector3, Vector3Int vectorMin, Vector3Int vectorMax, params Vector3Int[] vectorsExclude)
    {
        var x = Random.Range(vectorMin.x, vectorMax.x);
        var y = Random.Range(vectorMin.y, vectorMax.y);
        var z = Random.Range(vectorMin.z, vectorMax.z);
        var randomVector = new Vector3Int(x, y, z);
        for (int i = 0; i < vectorsExclude.Length; i++)
        {
            if (randomVector==vectorsExclude[i])
            {
                return randomVector.GetRandomVectorInt(vectorMin,vectorMax,vectorsExclude);
            }
        }
        return randomVector;
    }

}
