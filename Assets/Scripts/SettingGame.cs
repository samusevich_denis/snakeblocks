﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SettingGame
{
    public static int countEnemy  =2;
    public static int extraLengthPlayer  = 0;
    public static int extraLengthEnemy = 1;
    public static float speedCycle  = 7f;
    public static int halfWightField = 12;
    public static float timeRespawnFruit = 20;
}
