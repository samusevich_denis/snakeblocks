﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class CheckDirectionNode : Node
    {
        [SerializeField] private SnakeAI snake;
        public override NodeState Evaluate()
        {
            //Debug.Log(this.gameObject.name);
            {
                //Debug.Log($"{snake.PositionHeadSnake} time {Time.time}");
                var newPositionSnake = SnakeCreator.GetNeighboringPoint(snake.PositionHeadSnake, snake.directionMove);
                if (LevelGenerator.CheckPositionIsPossible(newPositionSnake))
                {
                    //Debug.Log($"time {Time.time}");
                    //Debug.Log($"{gameObject.name} time: {Time.time} ");
                    snake.PositionHeadSnake = newPositionSnake;
                    return NodeState.Success;
                }
                return NodeState.Failure;
            }
        }

    }
}
