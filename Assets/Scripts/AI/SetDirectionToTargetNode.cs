﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class SetDirectionToTargetNode : SetDirectionToFruitNode
    {
        public override void FindTarget()
        {
            var snakeEnemy = (SnakeAIEnemy)snake;
            pointTarget = snakeEnemy.pointTarget;
        }
    }

}