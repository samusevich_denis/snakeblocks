﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField] private GameObject prefabFruit;
    [SerializeField] private UIController uIController;
    [SerializeField] private GameObject snake;
    [SerializeField] private GameObject snakeEnemy;

    public static bool IsGameOver { get; set; }
    private static int lengthSnake;
    public static int LengthSnake
    {
        get => lengthSnake; set
        {
            lengthSnake = value;
            UIController.SetLengthUI(lengthSnake);
        }
    }
    private AI.SnakeAI snakeAI;
    private AI.SnakeAI[] snakesAIEnemy = new AI.SnakeAI[SettingGame.countEnemy];
    public static float Cycle  { get; private set; }
    public static bool IsChangeState { get; private set; }
    void Start()
    {
        FruitCreator.PrefabFruit = prefabFruit;
        LevelGenerator.Activation();
        uIController.Activation(this);

        snakeAI = Instantiate(snake, Vector3.zero, Quaternion.identity).GetComponent<AI.SnakeAI>();;
        for (int i = 0; i < SettingGame.countEnemy; i++)
        {
            snakesAIEnemy[i] = Instantiate(snakeEnemy, Vector3.zero, Quaternion.identity).GetComponent<AI.SnakeAI>();
        }
        LengthSnake = 2 + SettingGame.extraLengthPlayer;
        IsGameOver = true;
    }

    void Update()
    {
        if (IsGameOver)
        {
            return;
        }
        if (IsChangeState)
        {
            IsChangeState = false;
        }
        if (Cycle > 1)
        {
            IsChangeState = true;
            Cycle %= 1;
        }
        Cycle += Time.deltaTime * SettingGame.speedCycle;
    }
    public static void SnakePlayerIsDie()
    {
        IsGameOver = true;
        UIController.ActivatePanelDie();
    }

    public void StartGame()
    {
        IsGameOver = false;
        LevelGenerator.ClearPointsExclude();
        LengthSnake = 2 + SettingGame.extraLengthPlayer;
        FruitCreator.CreateFruit();
        snakeAI.RestartSnake();
        for (int i = 0; i < snakesAIEnemy.Length; i++)
        {
            snakesAIEnemy[i].RestartSnake();
        }
    }

    public void RestarGame()
    {
        FruitCreator.ClearFruit();
        StartGame();
    }
}
