﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace AI
{
    public class SnakeAIEnemy : SnakeAI
    {
        [HideInInspector] public Vector3Int pointTarget;
        protected override void Activate()
        {
            LevelGenerator.GetVectorsFieldBoundary(out Vector3Int vectorMin, out Vector3Int vectorMax);
            pointTarget = pointTarget.GetRandomVectorInt(vectorMin, vectorMax, LevelGenerator.PointsExclude.ToArray());
            base.Activate();
            extraLength = SettingGame.extraLengthEnemy;
        }
        public override void RestartSnake()
        {
            isSnakeDie = false;
            Activate();
        }
        protected override void Update()
        {
            if (GameController.IsGameOver)
            {
                if (!isSnakeDie)
                {
                    SnakeDie();
                }
                return;
            }
            IsAte = false;
            if (isSnakeDie)
            {
                RestartSnake();
                return;
            }
            if (GameController.IsChangeState)
            {
                if (pointTarget == positionHeadSnake)
                {
                    LevelGenerator.GetVectorsFieldBoundary(out Vector3Int vectorMin, out Vector3Int vectorMax);
                    pointTarget = pointTarget.GetRandomVectorInt(vectorMin, vectorMax, LevelGenerator.PointsExclude.ToArray());
                    IsAte = true;
                }
                bool isExtraLength = false;
                if (extraLength > 0 && IsAte == false)
                {
                    isExtraLength = true;
                    extraLength--;
                }
                snakeTail.SetPositionBody(positionHeadSnake, directionMove, isExtraLength);
            }
            rootNode.Evaluate();
            if (GameController.IsChangeState)
            { 
                if (isSnakeDie)
                {
                    return;
                }
                LevelGenerator.AddPointsExclude(positionHeadSnake);
            }
        }

        public override void SnakeDie()
        {
            SnakeDestroy();
        }
    }
}

