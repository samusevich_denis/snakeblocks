﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class SetDirectionToFruitNode : Node
    {
        [SerializeField] protected SnakeAI snake;
        protected Vector3Int pointTarget;
        private float timeFindFruit = 5;
        private float timerFindFruit;
        private bool isFindTarget;
        private int indexTarget;

        public virtual void FindTarget()
        {
            isFindTarget = false;
            float distance = float.MaxValue;
            for (int i = 0; i < FruitCreator.PointsFruits.Count; i++)
            {
                if (FruitCreator.Fruits[i].activeSelf)
                {
                    var newDistance = Vector3Int.Distance(FruitCreator.PointsFruits[i], snake.PositionHeadSnake);
                    if (distance > newDistance)
                    {
                        distance = newDistance;
                        pointTarget = FruitCreator.PointsFruits[i];
                        indexTarget = i;
                        isFindTarget = true;
                    }
                }
            }
        }

        public override NodeState Evaluate()
        {
            if (!isFindTarget)
            {
                FindTarget();
            }
            if (!FruitCreator.Fruits[indexTarget].activeSelf)
            {
                isFindTarget = false;
            }
            if (Vector3.Distance(FruitCreator.Fruits[indexTarget].transform.position,pointTarget)>0.9f)
            {
                isFindTarget = false;
            }

            // if (!UTurn())
            // {
            var random = Random.Range(0, 2);
            if (snake.PositionHeadSnake.x == pointTarget.x)
            {
                random = 1;
            }
            if (snake.PositionHeadSnake.y == pointTarget.y)
            {
                random = 0;
            }
            if (random == 0)
            {
                if (snake.PositionHeadSnake.x > pointTarget.x)
                {
                    snake.directionMove = snake.directionMove == SnakeCreator.InversionDirection(Direction.Left) ? SnakeCreator.RandomDirection(Direction.Up, Direction.Down) : Direction.Left;
                }
                else
                {
                    snake.directionMove = snake.directionMove == SnakeCreator.InversionDirection(Direction.Right) ? SnakeCreator.RandomDirection(Direction.Up, Direction.Down) : Direction.Right;
                }
            }
            else
            {
                if (snake.PositionHeadSnake.y > pointTarget.y)
                {
                    snake.directionMove = snake.directionMove == SnakeCreator.InversionDirection(Direction.Down) ? SnakeCreator.RandomDirection(Direction.Left, Direction.Right) : Direction.Down;
                }
                else
                {
                    snake.directionMove = snake.directionMove == SnakeCreator.InversionDirection(Direction.Up) ? SnakeCreator.RandomDirection(Direction.Left, Direction.Right) : Direction.Up;
                }
            }
            // }
            return NodeState.Success;
        }

        // private bool UTurn()
        // {
        //     if (pointTarget.x == snake.PositionHeadSnake.x)
        //     {
        //         if (snake.PositionHeadSnake.y > pointTarget.y && snake.directionMove == Direction.Up || snake.PositionHeadSnake.y < pointTarget.y && snake.directionMove == Direction.Down)
        //         {
        //             var random = Random.Range(0, 2);
        //             snake.directionMove = random == 0 ? Direction.Right : Direction.Left;
        //             return true;
        //         }
        //     }
        //     if (pointTarget.y == snake.PositionHeadSnake.y)
        //     {
        //         if (snake.PositionHeadSnake.x > pointTarget.x && snake.directionMove == Direction.Right || snake.PositionHeadSnake.x < pointTarget.x && snake.directionMove == Direction.Left)
        //         {
        //             var random = Random.Range(0, 2);
        //             snake.directionMove = random == 0 ? Direction.Up : Direction.Down;
        //             return true;
        //         }
        //     }
        //     return false;
        // }
    }
}

