﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class FruitCreator
{
    public static GameObject PrefabFruit { get; set; }
    public static int CountFruit { get; private set; }
    public static List<Vector3Int> PointsFruits { get; private set; } =  new List<Vector3Int>();
    public static List<GameObject> Fruits { get; private set; }= new List<GameObject>();

    public static void SetDeactivateFruit(int indexFruit)
    {
        CountFruit--;
        Fruits[indexFruit].SetActive(false);
    }
    public static void ClearFruit()
    {
        for (int i = 0; i < Fruits.Count; i++)
        {
            Fruits[i].SetActive(false);
        }
        CountFruit = 0;
    }
    public static void CreateFruit()
    {
        CountFruit++;
        int indexFruit = -1;
        for (int i = 0; i < Fruits.Count; i++)
        {
            if (Fruits[i].activeSelf == false)
            {
                indexFruit = i;
            }
        }
        if (indexFruit == -1)
        {
            Fruits.Add(GameObject.Instantiate(PrefabFruit));
            PointsFruits.Add(Vector3Int.zero);
            Fruits[Fruits.Count - 1].SetActive(false);
            indexFruit = Fruits.Count - 1;
        }
        LevelGenerator.GetVectorsFieldBoundary(out Vector3Int vectorMinLeftDown, out Vector3Int vectorMaxRightUp);
        PointsFruits[indexFruit] = PointsFruits[indexFruit].GetRandomVectorInt(vectorMinLeftDown, vectorMaxRightUp, LevelGenerator.PointsExclude.ToArray());
        Fruits[indexFruit].transform.position = PointsFruits[indexFruit];
        Fruits[indexFruit].SetActive(true);
    }
}
