﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField] private Text lengthPlayerSnake;
    [SerializeField] private GameObject panelDie;
    private static Text LengthPlayerSnake { get; set; }
    private static GameObject PanelDie { get; set; }
    private GameController gameController;

    public void Activation(GameController gameController)
    {
        LengthPlayerSnake = lengthPlayerSnake;
        PanelDie = panelDie;
        this.gameController = gameController;
    }
    
    public void ExitGame()
    {
        Application.Quit();
    }
    public void StartGame()
    {
        gameController.StartGame();
    }
    public void RestartGame()
    {
        gameController.RestarGame();
    }

    public static void SetLengthUI(int length)
    {
        LengthPlayerSnake.text = length.ToString();
    }
    public static void ActivatePanelDie()
    {
        PanelDie.SetActive(true);
    }
}
