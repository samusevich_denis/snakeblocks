﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AI
{
    public class SetDirectionNode : Node
    {
        [SerializeField] private SnakeAI snake;
        public override NodeState Evaluate()
        {
            //Debug.Log(this.gameObject.name);
            if (SnakeCreator.TryGetRandomNeighboringPosition(snake.PositionHeadSnake, SnakeCreator.GetPossibleDirections(snake.lastDirection), out Vector3Int newPositionHeadSnake, out Direction newDirection))
            {
                snake.directionMove = newDirection;
                snake.PositionHeadSnake = newPositionHeadSnake;
                return NodeState.Success;
            }
            snake.PositionHeadSnake = SnakeCreator.GetNeighboringPoint(snake.PositionHeadSnake, snake.directionMove);
            return NodeState.Failure;
        }

    }
}

