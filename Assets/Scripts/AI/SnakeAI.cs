﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public class SnakeAI : MonoBehaviour
    {
        [SerializeField] protected GameObject prefabCube;
        [SerializeField] protected Node rootNode;
        protected int extraLength;
        protected SnakeBody snakeTail;
        public bool IsAte { get; protected set; }
        protected bool isSnakeDie = true;
        [HideInInspector] public Direction directionMove;
        [HideInInspector] public Direction lastDirection;
        [HideInInspector] public Transform headSnake;

        protected Vector3Int positionHeadSnake;
        public Vector3Int PositionHeadSnake
        {
            get => positionHeadSnake;
            set
            {
                positionHeadSnake = value;
            }
        }

        protected virtual void Activate()
        {
            extraLength = SettingGame.extraLengthPlayer;
            headSnake = SnakeCreator.CreateSnake(out positionHeadSnake, prefabCube, out snakeTail);
            //SnakeCreator.TryGetRandomNeighboringPosition(positionHeadSnake, new List<Direction>() { Direction.Up, Direction.Right, Direction.Down, Direction.Left }, out Vector3Int positionTailSnake, out directionMove);
            //LevelGenerator.RemovePointsExclude(positionHeadSnake);
        }
        public virtual void RestartSnake()
        {
            isSnakeDie = false;
            Activate();
        }

        protected virtual void Update()
        {
            if (GameController.IsGameOver)
            {
                return;
            }

            if (isSnakeDie)
            {
                return;
            }
            if (GameController.IsChangeState)
            {
                lastDirection = directionMove;
                for (int i = 0; i < FruitCreator.PointsFruits.Count; i++)
                {
                    if (FruitCreator.Fruits[i].activeSelf && positionHeadSnake == FruitCreator.PointsFruits[i])
                    {
                        GameController.LengthSnake++;
                        FruitCreator.SetDeactivateFruit(i);
                        IsAte = true;
                        break;
                    }
                }
                if (extraLength > 0 && IsAte == false)
                {
                    IsAte = true;
                    extraLength--;
                }
                snakeTail.SetPositionBody(positionHeadSnake, directionMove, IsAte);
                IsAte = false;
            }
            rootNode.Evaluate();
            if (GameController.IsChangeState)
            {
                if (isSnakeDie)
                {
                    return;
                }
                LevelGenerator.AddPointsExclude(positionHeadSnake);
            }
        }
        public virtual void SnakeDie()
        {
            SnakeDestroy();
            GameController.SnakePlayerIsDie();
        }
        protected void SnakeDestroy()
        {
            snakeTail.SnakeDie();
            headSnake.gameObject.AddComponent<Rigidbody>().AddForce(Random.onUnitSphere * 2, ForceMode.Impulse);
            Destroy(headSnake.gameObject, 5f);
            isSnakeDie = true;
            LevelGenerator.RemovePointsExclude(positionHeadSnake);
        }
    }
}



