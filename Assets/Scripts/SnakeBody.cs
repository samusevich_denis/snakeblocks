﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeBody : MonoBehaviour
{
    private Vector3Int positionBody;
    private Direction directionMove;
    private SnakeBody snakeTail;
    private bool isDie;
    private void Update()
    {
        if (isDie)
        {
            return;
        }
        var directionVector = SnakeCreator.GetDirectionVector(directionMove);
        transform.position = positionBody - (directionVector - (Vector3)directionVector * GameController.Cycle);
    }
    public void SetPositionBody(Vector3Int newPosition, Direction newDirection, bool isCreateTail)
    {
        if (Direction.None != newDirection)
        {
            if (snakeTail != null)
            {
                snakeTail.SetPositionBody(positionBody, directionMove, isCreateTail);
            }
            else
            {
                if (isCreateTail)
                {
                    var tail = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    tail.transform.position = positionBody;
                    snakeTail = tail.AddComponent<SnakeBody>();
                    snakeTail.SetPositionBody(positionBody, Direction.None, false);
                }
                else
                {
                    LevelGenerator.RemovePointsExclude(positionBody);
                }
            }
        }
        positionBody = newPosition;
        directionMove = newDirection;
    }
    public void SetSettingBody(Vector3Int newPosition, Direction newDirection)
    {
        positionBody = newPosition;
        directionMove = newDirection;
    }
    public void SnakeDie()
    {
        if (snakeTail != null)
        {
            snakeTail.SnakeDie();
        }
        isDie = true;
        gameObject.AddComponent<Rigidbody>();
        LevelGenerator.RemovePointsExclude(positionBody);
        Destroy(gameObject, 5f);
    }
}
