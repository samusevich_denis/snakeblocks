﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace AI
{
    public class DieNode : Node
    {
        [SerializeField] private SnakeAI snake;

        public override NodeState Evaluate()
        {
            //Debug.Log(this.gameObject.name);
            if (LevelGenerator.CheckPositionIsPossible(snake.PositionHeadSnake,snake.PositionHeadSnake))
            {
                return NodeState.Failure;
            }
            snake.SnakeDie();
            return NodeState.Success;
        }

    }
}

