﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum Direction
{
    Up,
    Right,
    Down,
    Left,
    None,
}
public static class SnakeCreator
{
    public static Vector3Int GetDirectionVector(Direction direction)
    {
        switch (direction)
        {
            case Direction.Up:
                {
                    return Vector3Int.up;
                }
            case Direction.Right:
                {
                    return Vector3Int.right;
                }
            case Direction.Down:
                {
                    return Vector3Int.down;
                }
            case Direction.Left:
                {
                    return Vector3Int.left;
                }
            default:
                return Vector3Int.zero;
        }
    }
    public static Vector3Int GetNeighboringPoint(Vector3Int point, Direction direction)
    {
        switch (direction)
        {
            case Direction.Up:
                {
                    return new Vector3Int(point.x, point.y + 1, point.z);
                }
            case Direction.Right:
                {
                    return new Vector3Int(point.x + 1, point.y, point.z);
                }
            case Direction.Down:
                {
                    return new Vector3Int(point.x, point.y - 1, point.z);
                }
            case Direction.Left:
                {
                    return new Vector3Int(point.x - 1, point.y, point.z);
                }
            default:
                Debug.LogWarning("no neighboring point");
                return Vector3Int.zero;
        }
    }
    public static Direction InversionDirection(Direction direction)
    {
        switch (direction)
        {
            case Direction.Up:
                {
                    return Direction.Down;
                }
            case Direction.Right:
                {
                    return Direction.Left;
                }
            case Direction.Down:
                {
                    return Direction.Up;
                }
            case Direction.Left:
                {
                    return Direction.Right;
                }
            default:
                Debug.LogWarning("None direction");
                return Direction.None;
        }
    }
    public static Direction RandomDirection(params Direction[] direction){
        var random = Random.Range(0,direction.Length);
        return direction[random];
    }
    public static List<Direction> GetPossibleDirections(Direction direction)
    {
        if (direction == Direction.Down || direction == Direction.Up)
        {
            return new List<Direction> { Direction.Left, Direction.Right, direction };
        }
        if (direction == Direction.Right || direction == Direction.Left)
        {
            return new List<Direction> { Direction.Up, Direction.Down, direction };
        }
        return new List<Direction>();
    }
    public static Transform CreateSnake(out Vector3Int positionHeadSnake, GameObject prefabCube, out SnakeBody snakeTail)
    {
        LevelGenerator.GetVectorsFieldBoundary(out Vector3Int vectorMin, out Vector3Int vectorMax);
        positionHeadSnake = Vector3Int.zero;
        positionHeadSnake = positionHeadSnake.GetRandomVectorInt(vectorMin, vectorMax, LevelGenerator.PointsExclude.ToArray());
        if (!TryGetRandomNeighboringPosition(positionHeadSnake, new List<Direction>() { Direction.Up, Direction.Right, Direction.Down, Direction.Left }, out Vector3Int positionTailSnake, out Direction directionHeadToTail))
        {
            Debug.LogWarning("You can't create a tail");
        }
        LevelGenerator.AddPointsExclude(positionHeadSnake);// PointsExclude.Add(positionHeadSnake);
        LevelGenerator.AddPointsExclude(positionTailSnake); //PointsExclude.Add(positionTailSnake);
        var head = GameObject.Instantiate(prefabCube, positionHeadSnake, Quaternion.identity); //GameObject.CreatePrimitive(PrimitiveType.Cube);
        var tail = GameObject.Instantiate(prefabCube, positionTailSnake, Quaternion.identity); //GameObject.CreatePrimitive(PrimitiveType.Cube);
        snakeTail = tail.AddComponent<SnakeBody>();
        var directionTailToHead = InversionDirection(directionHeadToTail);
        snakeTail.SetSettingBody(positionTailSnake, directionTailToHead);
        return head.transform;
    }
    public static bool TryGetRandomNeighboringPosition(Vector3Int startPosition, List<Direction> directions, out Vector3Int newPosition, out Direction startPositionToNewPosition)
    {
        if (directions.Count == 0)
        {
            newPosition = Vector3Int.zero;
            startPositionToNewPosition = Direction.None;
            return false;
        }
        var randomDirection = Random.Range(0, directions.Count);
        switch (directions[randomDirection])
        {
            case Direction.Up:
                {
                    newPosition = new Vector3Int(startPosition.x, startPosition.y + 1, startPosition.z);
                    startPositionToNewPosition = Direction.Up;
                    if (newPosition.y == LevelGenerator.PointRightUp.y || LevelGenerator.CheckPoint(newPosition))
                    {
                        break;
                    }
                    return true;
                }
            case Direction.Right:
                {
                    newPosition = new Vector3Int(startPosition.x + 1, startPosition.y, startPosition.z);
                    startPositionToNewPosition = Direction.Right;
                    if (newPosition.x == LevelGenerator.PointRightUp.x || LevelGenerator.CheckPoint(newPosition))
                    {
                        break;
                    }
                    return true;
                }
            case Direction.Down:
                {
                    newPosition = new Vector3Int(startPosition.x, startPosition.y - 1, startPosition.z);
                    startPositionToNewPosition = Direction.Down;
                    if (newPosition.y == LevelGenerator.PointLeftDown.y || LevelGenerator.CheckPoint(newPosition))
                    {
                        break;
                    }
                    return true;
                }
            case Direction.Left:
                {
                    newPosition = new Vector3Int(startPosition.x - 1, startPosition.y, startPosition.z);
                    startPositionToNewPosition = Direction.Left;
                    if (newPosition.x == LevelGenerator.PointLeftDown.x || LevelGenerator.CheckPoint(newPosition))
                    {
                        break;
                    }
                    return true;
                }
            default:
                Debug.LogWarning("No direction");
                newPosition = Vector3Int.zero;
                startPositionToNewPosition = Direction.None;
                return false;
        }
        //Debug.Log(startPositionToNewPosition);
        directions.RemoveAt(directions.IndexOf(directions[randomDirection]));
        return TryGetRandomNeighboringPosition(startPosition, directions, out newPosition, out startPositionToNewPosition);
    }
}
