﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitTimer : MonoBehaviour
{
    private float timerRespawnFruit = SettingGame.timeRespawnFruit;
    private void Update()
    {
        if (GameController.IsGameOver)
        {
            return;
        }
        if (FruitCreator.CountFruit == 0)
        {
            FruitCreator.CreateFruit();
        }

        if (timerRespawnFruit < 0)
        {
            FruitCreator.CreateFruit();
            timerRespawnFruit = SettingGame.timeRespawnFruit;
        }
        timerRespawnFruit -= Time.deltaTime;
    }
}
