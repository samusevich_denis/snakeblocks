﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class LevelGenerator : MonoBehaviour
{
    public static List<Vector3Int> PointsExclude = new List<Vector3Int>();
    public static List<GameObject> cube = new List<GameObject>();
    public static Vector3Int PointLeftDown { get; private set; }
    public static Vector3Int PointRightUp { get; private set; }

    private static int CountRemove;
    private int CountDelete;
    public static void Activation()
    {
        var cameraGame = Camera.main;
        Ray ray = cameraGame.ScreenPointToRay(Vector3.zero);
        float otn = Mathf.Abs(SettingGame.halfWightField / ray.direction.x);
        var point = ray.direction * otn;
        PointLeftDown = new Vector3Int((int)point.x + 1, (int)point.y + 1, (int)(point.z + cameraGame.transform.position.z));
        PointRightUp = new Vector3Int(-PointLeftDown.x, -PointLeftDown.y, PointLeftDown.z);
        CreateField();
    }
    private static void CreateField()
    {
        CreateLine(PointLeftDown.y, PointRightUp.y, PointLeftDown.x, PointLeftDown.z, false, true);
        CreateLine(PointLeftDown.x, PointRightUp.x, PointRightUp.y, PointLeftDown.z, false, false);
        CreateLine(PointRightUp.y, PointLeftDown.y, PointRightUp.x, PointRightUp.z, true, true);
        CreateLine(PointRightUp.x, PointLeftDown.x, PointLeftDown.y, PointRightUp.z, true, false);
    }
    private static void CreateLine(int from, int to, int axis, int positionZ, bool isReverse, bool isXLine)
    {
        for (int i = isReverse ? from - 1 : from + 1; isReverse ? i >= to : i <= to; i += isReverse ? -1 : +1)
        {
            var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.transform.position = isXLine ? new Vector3(axis, i, positionZ) : new Vector3(i, axis, positionZ);
        }
    }

    public static void ClearPointsExclude()
    {
        PointsExclude.Clear();
    }
    public static void AddPointsExclude(Vector3Int point)
    {
        //Debug.Log($"{point} add to PointsExclude time: {Time.time}");
        // var obj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        // obj.transform.position = new Vector3Int(point.x, point.y + 20, point.z);
        // cube.Add(obj);
        PointsExclude.Add(point);
    }
    public static void RemovePointsExclude(Vector3Int point)
    {

        // for (int i = 0; i < PointsExclude.Count; i++)
        // {
        //     if (point == PointsExclude[i])
        //     {
        //         Debug.Log($"index {i} posit = {cube[i].transform.position}, == {point} " );
        //         cube[i].transform.position = new Vector3Int(50,50,50);
        //         cube.RemoveAt(i);
        //         break;
        //     }
        // }
        // Debug.Log($"remove {point}");
        PointsExclude.Remove(point);
    }

    public static void GetVectorsFieldBoundary(out Vector3Int vectorMinLeftDown, out Vector3Int vectorMaxRightUp)
    {
        vectorMinLeftDown = new Vector3Int(LevelGenerator.PointLeftDown.x + 1, LevelGenerator.PointLeftDown.y + 1, LevelGenerator.PointLeftDown.z);
        vectorMaxRightUp = PointRightUp;
    }
    public static bool CheckPositionIsPossible(Vector3Int newPositionSnake)
    {
        if (CheckPoint(newPositionSnake) || CheckField(newPositionSnake))
        {
            return false;
        }
        return true;
    }
    public static bool CheckPositionIsPossible(Vector3Int newPositionSnake, Vector3Int positionExclude)
    {
        if (CheckPoint(newPositionSnake, positionExclude) || CheckField(newPositionSnake))
        {
            return false;
        }
        return true;
    }
    public static bool CheckPoint(Vector3Int point, Vector3Int positionExclude)
    {
        var CopyPointsExclude = new List<Vector3Int>(PointsExclude);
        CopyPointsExclude.Remove(positionExclude);
        for (int i = 0; i < CopyPointsExclude.Count; i++)
        {
            if (point == CopyPointsExclude[i])
            {
                //Debug.Log($"{point} == {PointsExclude[i]} time: {Time.time}");
                return true;
            }
        }
        return false;
    }
    public static bool CheckPoint(Vector3Int point)
    {
        for (int i = 0; i < PointsExclude.Count; i++)
        {
            if (point == PointsExclude[i])
            {
                //Debug.Log($"{point} == {PointsExclude[i]} time: {Time.time}");
                return true;
            }
        }
        return false;
    }
    public static bool CheckField(Vector3Int point)
    {
        if (point.x == PointLeftDown.x
            || point.x == PointRightUp.x
            || point.y == PointLeftDown.y
            || point.y == PointRightUp.y)
        {
            //Debug.Log($"{point} == {PointLeftDown} or {PointRightUp}");
            return true;
        }
        return false;
    }
}
